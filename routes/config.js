/*jslint node: true */
/*jslint nomen: true */
"use strict";

var PROD = 'production', STAGING = 'staging', LOCAL = 'local';
var MODE = ((process.env.NODE_ENV == PROD) && PROD) || ((process.env.NODE_ENV == STAGING) && STAGING) || LOCAL;
var IS_PROD = (MODE == PROD), IS_STAGING = (MODE == STAGING), IS_LOCAL = (MODE == LOCAL);
var PROD_HOST = '', LOCALHOST = 'localhost';

exports.getMode = getMode;
exports.getHost = getHost;
exports.getExpressPort = getExpressPort;
exports.getESPort = getESPort;
exports.sessionSecret = 'local shopping';

function getMode() {
    return MODE;
}

function getHost() {
    if (MODE == PROD) {
        return PROD_HOST;
    }
    return LOCALHOST;
}

function getExpressPort() {
    return 3000;
}

function getESPort() {
    return 9200;
}