/*jslint node: true */
/*jslint nomen: true */
"use strict";

var store = require('elasticsearch');
var config = require('./config.js');
var client = new store.Client({
    host : config.getHost() + ':' + config.getESPort(),
    apiVersion: "1.0"
});

exports.initSignup = initSignup;

function initSignup(phoneNumber, email, name, verifyCode) {
    client.index({
        index: 'localshopping',
        type: 'initsignups',
        id: phoneNumber,
        body: {
            phone : phoneNumber,
            email : email,
            name : name,
            verifyCode : verifyCode
        }
    }, function (error, response) {
        console.log(response);
    });
}