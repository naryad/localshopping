/*jslint node: true */
/*jslint nomen: true */
"use strict";

var store = require('./store.js');

exports.initSignup = initSignup;

function initSignup(phoneNumber, email, name) {
    console.log('Phone Number ' + phoneNumber);
    var verifyCode = Math.floor(Math.random()*9000) + 1000;
    store.initSignup(phoneNumber, email, name, verifyCode);
    return {'verifyCode' : verifyCode};
}