/*jslint node: true */
/*jslint nomen: true */
/*jslint es5: true */
"use strict";

var https = require('https'),
    http = require('http'),
    express = require('express'),
    app = express(),
    utils = require('./routes/utils.js'),
    config = require('./routes/config.js'),
    user = require('./routes/user.js'),
    auth = require('./routes/auth.js'),
    view = require('./views/view.js'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    path = require('path');

var hour = 3600000;
var day = (hour * 24);
var week = (day * 7);
var month = (day * 30);

app.set('port', config.getExpressPort());

app.use(cookieParser());
app.use(bodyParser());
app.use(session({
    secret: config.sessionSecret,
    name: 'sid',  
    cookie: { secure: true }
}));
/*
app.use(express.static(path.join(__dirname, 'public'), { maxAge: week }));
app.use(function(req, res) {
  res.status(404);
  res.render('404');
});
*/
app.post('/signup', view.initSignup);

var server = app.listen(app.get('port'), function() {
        console.log('Listening on port %d', server.address().port);
});
