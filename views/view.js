/*jslint node: true */
/*jslint nomen: true */
"use strict";

var auth = require('../routes/auth.js');

exports.initSignup = initSignup;

function initSignup(req, res) {
    console.log(JSON.stringify(req.body));
    var phoneNumber = req.body.phone, 
        email = req.body.email,
        name = req.body.name,
        verifyCode = Math.floor(Math.random()*9000) + 1000;
    var verify = auth.initSignup(phoneNumber, email, name);
    console.log(verify);
    res.send(200, verify);
}
