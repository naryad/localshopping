curl -XPUT 'http://localhost:9200/localshopping'

curl -XPUT 'http://localhost:9200/localshopping/initsignups/_mapping' -d '
{
    "initsignups" : {
        "properties" : {
            "phone" : {"type" : "string", "index" : "not_analyzed"},
            "email" : {"type" : "string", "index" : "not_analyzed"},
            "name" : {"type" : "string"},
            "verifyCode" : {"type" : "integer"}
        }
    }
}
'